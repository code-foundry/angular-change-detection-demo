import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';

import { RoutePath } from '#common/navigation';

interface MenuItem {
  label: string;
  route: Parameters<Router['navigate']>[0];
}

@Component({
  selector: 'app-menu-button',
  templateUrl: './menu-button.component.html',
  styleUrls: ['./menu-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuButtonComponent {
  public readonly menuItems: MenuItem[] = [
    { label: '1 - The need for change detection', route: [RoutePath.NeedForChangeDetection] },
    { label: '2 - Async task change detection trigger', route: [RoutePath.AsyncTaskChangeDetectionTrigger] },
    { label: '3 - Input change detection trigger', route: [RoutePath.InputChangeDetectionTrigger] },
    { label: '4 - Output change detection trigger', route: [RoutePath.OutputChangeDetectionTrigger] },
    { label: '5 - Change detection across components', route: [RoutePath.ChangeDetectionAcrossComponents] },
    { label: '6 - Change detection propagation (default)', route: [RoutePath.ChangeDetectionPropgation] },
    { label: '7 - Change detection propagation (on push)', route: [RoutePath.ChangeDetectionPropgationOnPush] },
    { label: '8 - Change detection propagation (detached)', route: [RoutePath.ChangeDetectionPropgationDetached] },
    { label: '9 - Change detection frenzy', route: [RoutePath.ChangeDetectionFrenzy] },
    { label: '10 - View model example', route: [RoutePath.ViewModelExample] },
    { label: '11 - Combining inputs and view models', route: [RoutePath.CombiningInputsAndViewModels] },
  ];
}
