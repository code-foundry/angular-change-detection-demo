import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { RouterModule } from '@angular/router';

import { MenuButtonComponent } from './menu-button.component';

@NgModule({
  imports: [CommonModule, RouterModule, MatButtonModule, MatIconModule, MatMenuModule],
  declarations: [MenuButtonComponent],
  exports: [MenuButtonComponent],
})
export class MenuModule {}
