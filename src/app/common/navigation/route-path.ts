export const RoutePath = {
  NeedForChangeDetection: 'need-for-change-detection',
  AsyncTaskChangeDetectionTrigger: 'async-task-change-detection-trigger',
  InputChangeDetectionTrigger: 'input-change-detection-trigger',
  OutputChangeDetectionTrigger: 'output-change-detection-trigger',
  ChangeDetectionAcrossComponents: 'change-detection-across-components',
  ChangeDetectionPropgation: 'change-detection-propagation',
  ChangeDetectionPropgationOnPush: 'change-detection-propagation-on-push',
  ChangeDetectionPropgationDetached: 'change-detection-propagation-detached',
  ChangeDetectionFrenzy: 'change-detection-frenzy',
  ViewModelExample: 'view-model-example',
  CombiningInputsAndViewModels: 'combining-inputs-and-view-models',
};
