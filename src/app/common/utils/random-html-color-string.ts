export function randomHtmlColorString(): string {
  return `#${Math.floor(Math.random() * Math.pow(2, 24))
    .toString(16)
    .padStart(6, '0')}`;
}
