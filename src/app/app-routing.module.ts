import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoutePath } from '#common/navigation';

const routes: Routes = [
  {
    path: RoutePath.NeedForChangeDetection,
    loadChildren: () => import('./features/need-for-change-detection').then((module) => module.NeedForChangeDetectionModule),
  },
  {
    path: RoutePath.AsyncTaskChangeDetectionTrigger,
    loadChildren: () =>
      import('./features/async-task-change-detection-trigger').then((module) => module.AsyncTaskChangeDetectionTriggerModule),
  },
  {
    path: RoutePath.InputChangeDetectionTrigger,
    loadChildren: () => import('./features/input-change-detection-trigger').then((module) => module.InputChangeDetectionTriggerModule),
  },
  {
    path: RoutePath.OutputChangeDetectionTrigger,
    loadChildren: () => import('./features/output-change-detection-trigger').then((module) => module.OutputChangeDetectionTriggerModule),
  },
  {
    path: RoutePath.ChangeDetectionAcrossComponents,
    loadChildren: () =>
      import('./features/change-detection-across-components').then((module) => module.ChangeDetectionAcrossComponentsModule),
  },
  {
    path: RoutePath.ChangeDetectionPropgation,
    loadChildren: () => import('./features/change-detection-propagation').then((module) => module.ChangeDetectionPropagationModule),
  },
  {
    path: RoutePath.ChangeDetectionPropgationOnPush,
    loadChildren: () =>
      import('./features/change-detection-propagation-on-push').then((module) => module.ChangeDetectionPropagationOnPushModule),
  },
  {
    path: RoutePath.ChangeDetectionPropgationDetached,
    loadChildren: () =>
      import('./features/change-detection-propagation-detached').then((module) => module.ChangeDetectionPropagationDetachedModule),
  },
  {
    path: RoutePath.ChangeDetectionFrenzy,
    loadChildren: () => import('./features/change-detection-frenzy').then((module) => module.ChangeDetectionFrenzyModule),
  },
  {
    path: RoutePath.ViewModelExample,
    loadChildren: () => import('./features/view-model-example').then((module) => module.ViewModelExampleModule),
  },
  {
    path: RoutePath.CombiningInputsAndViewModels,
    loadChildren: () => import('./features/combining-inputs-and-view-models').then((module) => module.CombiningInputsAndViewModelsModule),
  },
  {
    path: '**',
    redirectTo: RoutePath.NeedForChangeDetection,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
