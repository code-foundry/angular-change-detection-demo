import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';

import { Example1Component } from './example-1.component';
import { Example2Component } from './example-2.component';
import { Example3Component } from './example-3.component';
import { Example4Component } from './example-4.component';
import { Example5Component } from './example-5.component';
import { Example6Component } from './example-6.component';
import { Example7Component } from './example-7.component';
import { Example8Component } from './example-8.component';
import { Example9Component } from './example-9.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: Example1Component }]), MatButtonModule],
  declarations: [
    Example1Component,
    Example2Component,
    Example3Component,
    Example4Component,
    Example5Component,
    Example6Component,
    Example7Component,
    Example8Component,
    Example9Component,
  ],
})
export class ChangeDetectionPropagationModule {}
