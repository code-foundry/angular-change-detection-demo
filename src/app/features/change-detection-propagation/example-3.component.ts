import { Component } from '@angular/core';

import { randomHtmlColorString } from '#common/utils/random-html-color-string';

@Component({
  selector: 'app-example-3',
  template: `
    <div [style.border-color]="borderColor">
      <div class="component-info">
        <div class="title">Component <strong>3</strong></div>
        <div class="change-detection-mode-label">(default)</div>
        <button mat-stroked-button (click)="triggerChangeDetection()">Trigger CD</button>
      </div>
      <div class="children">
        <app-example-4></app-example-4>
        <app-example-5></app-example-5>
      </div>
    </div>
  `,
  styleUrls: ['./example.component.scss'],
})
export class Example3Component {
  public get borderColor(): string {
    return randomHtmlColorString();
  }

  public triggerChangeDetection(): void {
    // Angular will do this for us :)
  }
}
