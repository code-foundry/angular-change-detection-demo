import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';

import { NeedForChangeDetectionComponent } from './need-for-change-detection.component';

@NgModule({
  imports: [MatButtonModule, RouterModule.forChild([{ path: '', component: NeedForChangeDetectionComponent }])],
  declarations: [NeedForChangeDetectionComponent],
})
export class NeedForChangeDetectionModule {}
