import { Component } from '@angular/core';

@Component({
  selector: 'app-need-for-change-detection',
  template: `
    <p>
      <button mat-raised-button color="primary" (click)="updateValue()">Update value</button>
    </p>

    <p>
      Value 1 = <strong>{{ value1 }}</strong>
    </p>

    <p>
      Value 2 = <strong>{{ value2 }}</strong>
    </p>

    <p>
      Product = <strong>{{ value1 * value2 }}</strong>
    </p>
  `,
})
export class NeedForChangeDetectionComponent {
  public value1 = 0;
  public value2 = 0;

  public updateValue(): void {
    this[Math.random() < 0.5 ? 'value1' : 'value2']++;
  }
}
