import { ChangeDetectionStrategy, Component } from '@angular/core';

import { randomHtmlColorString } from '#common/utils/random-html-color-string';

@Component({
  selector: 'app-example-8',
  template: `
    <div [style.border-color]="borderColor">
      <div class="component-info">
        <div class="title">Component <strong>8</strong></div>
        <div class="change-detection-mode-label">(default)</div>
        <button mat-stroked-button (click)="triggerChangeDetection()">Trigger CD</button>
      </div>
      <div class="children"></div>
    </div>
  `,
  styleUrls: ['./example.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
})
export class Example8Component {
  public get borderColor(): string {
    return randomHtmlColorString();
  }

  public triggerChangeDetection(): void {
    // Angular will do this for us :)
  }
}
