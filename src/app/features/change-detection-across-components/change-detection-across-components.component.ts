import { Component } from '@angular/core';

@Component({
  selector: 'app-change-detection-across-components',
  template: `
    <app-prepender></app-prepender>
    <app-appender></app-appender>
  `,
})
export class ChangeDetectionAcrossComponentsComponent {}
