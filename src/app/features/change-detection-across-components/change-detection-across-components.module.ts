import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';

import { AppenderComponent } from './appender.component';
import { ChangeDetectionAcrossComponentsComponent } from './change-detection-across-components.component';
import { PrependerComponent } from './prepender.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: ChangeDetectionAcrossComponentsComponent }]), MatButtonModule],
  declarations: [AppenderComponent, ChangeDetectionAcrossComponentsComponent, PrependerComponent],
})
export class ChangeDetectionAcrossComponentsModule {}
