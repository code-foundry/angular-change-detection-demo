import { Component } from '@angular/core';

import { SHARED_OBJECT } from './shared-object';

@Component({
  selector: 'app-appender',
  template: `
    <p><button mat-raised-button color="accent" (click)="append()">Append</button></p>

    <p>
      Value = <strong>{{ sharedObject.value }}</strong>
    </p>
  `,
})
export class AppenderComponent {
  public sharedObject = SHARED_OBJECT; // Disclaimer: this is bad practice! Use DI or @Input instead!

  public append(): void {
    const character = String.fromCharCode(65 + Math.floor(Math.random() * 26));

    this.sharedObject.value = `${this.sharedObject.value}${character}`;
  }
}
