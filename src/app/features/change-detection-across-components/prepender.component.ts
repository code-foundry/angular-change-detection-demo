import { Component } from '@angular/core';

import { SHARED_OBJECT } from './shared-object';

@Component({
  selector: 'app-prepender',
  template: `
    <p><button mat-raised-button color="primary" (click)="prepend()">Prepend</button></p>

    <p>
      Value = <strong>{{ sharedObject.value }}</strong>
    </p>
  `,
})
export class PrependerComponent {
  public sharedObject = SHARED_OBJECT; // Disclaimer: this is bad practice! Use DI or @Input instead!

  public prepend(): void {
    const character = String.fromCharCode(65 + Math.floor(Math.random() * 26));

    this.sharedObject.value = `${character}${this.sharedObject.value}`;
  }
}
