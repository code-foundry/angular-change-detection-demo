import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';

import { Animal } from './animal.model';
import { ANIMALS } from './animals';
import { EmojiService } from './emoji.service';

@Component({
  selector: 'app-animal-listing',
  template: `
    <h1>Adopt a pet:</h1>

    <div *ngFor="let animal of animals" class="animal-item">
      <button mat-mini-fab color="accent">
        {{ getEmojiForAnimal(animal) | async }}
      </button>
      <span>
        {{ animal.name }}
      </span>
    </div>

    <p>
      Change detection indicator: <strong>{{ value }}</strong>
    </p>
  `,
  styleUrls: ['./animal-listing.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AnimalListingComponent {
  public readonly animals = ANIMALS;

  constructor(private readonly emojiService: EmojiService) {}

  public getEmojiForAnimal(animal: Animal): Observable<string> {
    console.log(new Date().toISOString(), '- Retrieving emoji for:', animal);

    return this.emojiService.getAnimalEmoji(animal);
  }

  public get value(): number {
    return Math.random();
  }
}
