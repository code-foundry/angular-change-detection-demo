import { Injectable } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { concatMap, delay, repeat } from 'rxjs/operators';

import { Animal } from './animal.model';

const ANIMAL_EMOJI_MAP = new Map<string, string[]>([
  ['insect', ['🐝', '🐜', '🐞', '🦋']],
  ['dog', ['🐕', '🐶']],
  ['fish', ['🐟', '🐠', '🐡']],
  ['rodent', ['🐁', '🐀']],
  ['dino', ['🦕', '🦖']],
]);

@Injectable()
export class EmojiService {
  public getAnimalEmoji(animal: Animal): Observable<string> {
    const emojis = ANIMAL_EMOJI_MAP.get(animal.id) ?? ['?'];

    return from(emojis).pipe(
      concatMap((emoji) => of(emoji).pipe(delay(Math.random() * 1700 + 300))),
      repeat(),
    );
  }
}
