import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-change-detection-frenzy',
  template: `<app-animal-listing></app-animal-listing>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChangeDetectionFrenzyComponent {}
