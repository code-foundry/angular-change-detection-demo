import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';

import { AnimalListingComponent } from './animal-listing.component';
import { ChangeDetectionFrenzyComponent } from './change-detection-frenzy.component';
import { EmojiService } from './emoji.service';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: ChangeDetectionFrenzyComponent }]), CommonModule, MatButtonModule],
  declarations: [AnimalListingComponent, ChangeDetectionFrenzyComponent],
  providers: [EmojiService],
})
export class ChangeDetectionFrenzyModule {}
