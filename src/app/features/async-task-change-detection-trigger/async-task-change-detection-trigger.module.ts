import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';

import { AsyncTaskChangeDetectionTriggerComponent } from './async-task-change-detection-trigger.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: AsyncTaskChangeDetectionTriggerComponent }]), MatButtonModule],
  declarations: [AsyncTaskChangeDetectionTriggerComponent],
})
export class AsyncTaskChangeDetectionTriggerModule {}
