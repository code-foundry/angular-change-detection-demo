import { ChangeDetectorRef, Component } from '@angular/core';

@Component({
  selector: 'app-async-task-change-detection-trigger',
  template: `
    <p><button mat-raised-button color="primary" (click)="updateValueDelayed()">Update value after 1 second</button></p>
    <p><button mat-raised-button color="primary" (click)="loadDataFromApi()">Load data from API</button></p>
    <p>
      text = <strong>{{ text }}</strong>
    </p>
  `,
})
export class AsyncTaskChangeDetectionTriggerComponent {
  public text = '';

  public updateValueDelayed(): void {
    this.text = '(waiting to be updated...)';

    setTimeout(() => {
      this.text = Date.now().toString(16);
    }, 1000);
  }

  public loadDataFromApi(): void {
    this.text = '(loading data...)';

    // Note, normally you would use Angular's HttpClient for this. XMLHttpRequest is used here to demonstrate change detection triggers via
    // async tasks that are intercepted via Zone.js
    const xhr = new XMLHttpRequest();
    xhr.open('get', 'https://reqres.in/api/users/1?delay=1');
    xhr.addEventListener('load', () => {
      this.text = xhr.responseText;
    });
    xhr.send();
  }
}
