import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { Animal } from './animal.model';
import { ANIMALS } from './animals';

const MAX_SUBSET_SIZE = 3;

@Component({
  selector: 'app-combining-inputs-and-view-models',
  template: `
    <app-animal-listing [animals]="animalSet" [numberOfAnimalsToShow]="numberOfAnimals.value"></app-animal-listing>

    <h2>Settings</h2>

    <mat-form-field appearance="fill">
      <mat-label>Number of animals</mat-label>
      <input matInput type="number" [formControl]="numberOfAnimals" />
    </mat-form-field>

    <p><button mat-raised-button color="primary" (click)="swapAnimalSet()">Swap animal set</button></p>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CombiningInputsAndViewModelsComponent {
  public animalSubsetIndex = 0;
  public animalSet: Animal[] = this.getAnimalSubset(this.animalSubsetIndex);
  public numberOfAnimals = new FormControl(3, Validators.required);

  public swapAnimalSet(): void {
    this.animalSubsetIndex = (this.animalSubsetIndex + 1) % Math.ceil(ANIMALS.length / MAX_SUBSET_SIZE);
    this.animalSet = this.getAnimalSubset(this.animalSubsetIndex);
  }

  private getAnimalSubset(index: number): Animal[] {
    return ANIMALS.slice(index * MAX_SUBSET_SIZE, (index + 1) * MAX_SUBSET_SIZE);
  }
}
