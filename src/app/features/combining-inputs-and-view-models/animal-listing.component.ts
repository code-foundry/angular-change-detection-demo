import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { observeProperty } from '#common/utils/observe-property';

import { Animal } from './animal.model';
import { EmojiService } from './emoji.service';

interface AnimalViewModel {
  name: string;
  emoji$: Observable<string>;
}

@Component({
  selector: 'app-animal-listing',
  template: `
    <h1>Adopt a pet:</h1>

    <div *ngFor="let animal of animalViewModels$ | async" class="animal-item">
      <button mat-mini-fab color="accent">
        {{ animal.emoji$ | async }}
      </button>
      <span>
        {{ animal.name }}
      </span>
    </div>
  `,
  styleUrls: ['./animal-listing.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AnimalListingComponent implements OnInit {
  @Input() public animals!: Animal[];
  @Input() public numberOfAnimalsToShow: number = 1;
  public animalViewModels$!: Observable<AnimalViewModel[]>;

  constructor(private readonly emojiService: EmojiService) {}

  public ngOnInit(): void {
    const animals$ = observeProperty(this, 'animals');
    const numberOfAnimalsToShow$ = observeProperty(this, 'numberOfAnimalsToShow');

    this.animalViewModels$ = combineLatest([animals$, numberOfAnimalsToShow$]).pipe(
      map(([animals, numberOfAnimalsToShow]) =>
        animals
          .map((animal) => ({
            name: animal.name,
            emoji$: this.emojiService.getAnimalEmoji(animal),
          }))
          .slice(0, numberOfAnimalsToShow),
      ),
    );
  }

  public get value(): number {
    return Math.random();
  }
}
