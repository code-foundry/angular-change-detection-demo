import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { RouterModule } from '@angular/router';

import { AnimalListingComponent } from './animal-listing.component';
import { CombiningInputsAndViewModelsComponent } from './combining-inputs-and-view-models.component';
import { EmojiService } from './emoji.service';

@NgModule({
  imports: [
    RouterModule.forChild([{ path: '', component: CombiningInputsAndViewModelsComponent }]),
    CommonModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  declarations: [AnimalListingComponent, CombiningInputsAndViewModelsComponent],
  providers: [EmojiService],
})
export class CombiningInputsAndViewModelsModule {}
