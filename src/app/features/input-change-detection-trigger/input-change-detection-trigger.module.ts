import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';

import { InputChangeDetectionTriggerComponent } from './input-change-detection-trigger.component';
import { ValueComponent } from './value.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: InputChangeDetectionTriggerComponent }]), MatButtonModule, MatIconModule],
  declarations: [InputChangeDetectionTriggerComponent, ValueComponent],
})
export class InputChangeDetectionTriggerModule {}
