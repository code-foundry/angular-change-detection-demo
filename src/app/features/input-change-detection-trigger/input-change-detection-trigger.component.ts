import { Component } from '@angular/core';

@Component({
  selector: 'app-input-change-detection-trigger',
  template: `
    <button mat-fab color="primary" (click)="increment()"><mat-icon>add</mat-icon></button>
    <button mat-fab color="accent" (click)="decrement()"><mat-icon>remove</mat-icon></button>
    <app-value [value]="value"></app-value>
  `,
  styleUrls: ['./input-change-detection-trigger.component.scss'],
})
export class InputChangeDetectionTriggerComponent {
  public value = 1;

  public increment(): void {
    this.value++;
  }

  public decrement(): void {
    this.value--;
  }
}
