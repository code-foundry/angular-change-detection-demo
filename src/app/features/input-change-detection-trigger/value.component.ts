import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-value',
  template: `value<sup>2</sup> = <strong>{{ value * value }}</strong>`,
})
export class ValueComponent {
  @Input() public value: number = 0;
}
