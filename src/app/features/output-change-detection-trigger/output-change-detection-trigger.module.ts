import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';

import { OutputChangeDetectionTriggerComponent } from './output-change-detection-trigger.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: OutputChangeDetectionTriggerComponent }]), MatButtonModule],
  declarations: [OutputChangeDetectionTriggerComponent],
})
export class OutputChangeDetectionTriggerModule {}
