import { Component } from '@angular/core';

@Component({
  selector: 'app-output-change-detection-trigger',
  template: `
    <p>
      <button mat-raised-button color="primary" (mouseenter)="doNothing()">Hover to trigger change detection</button>
    </p>
    <p>
      value = <strong>{{ value }}</strong>
    </p>
  `,
})
export class OutputChangeDetectionTriggerComponent {
  public doNothing(): void {
    // Ok, doing nothing...
  }

  public get value(): number {
    console.log('get value() called @', new Date().toISOString());

    return Math.random();
  }
}
