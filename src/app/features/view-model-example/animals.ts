import { Animal } from './animal.model';

export const ANIMALS: Animal[] = [
  { id: 'insect', name: 'Cute Insect' },
  { id: 'dog', name: 'Fluffy Dog' },
  { id: 'fish', name: 'Hungry Fish' },
  { id: 'rodent', name: 'Sneaky Rodent' },
  { id: 'dino', name: 'Dangerous Dino' },
];
