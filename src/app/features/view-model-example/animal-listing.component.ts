import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Animal } from './animal.model';
import { ANIMALS } from './animals';
import { EmojiService } from './emoji.service';

interface AnimalViewModel {
  name: string;
  emoji$: Observable<string>;
}

@Component({
  selector: 'app-animal-listing',
  template: `
    <h1>Adopt a pet:</h1>

    <div *ngFor="let animal of animals" class="animal-item">
      <button mat-mini-fab color="accent">
        {{ animal.emoji$ | async }}
      </button>
      <span>
        {{ animal.name }}
      </span>
    </div>

    <p>
      Change detection indicator: <strong>{{ value }}</strong>
    </p>
  `,
  styleUrls: ['./animal-listing.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AnimalListingComponent implements OnInit {
  public animals!: AnimalViewModel[];

  constructor(private readonly emojiService: EmojiService) {}

  public ngOnInit(): void {
    this.animals = ANIMALS.map((animal) => ({
      name: animal.name,
      emoji$: this.getEmojiForAnimal(animal),
    }));
  }

  public getEmojiForAnimal(animal: Animal): Observable<string> {
    console.log(new Date().toISOString(), '- Retrieving emoji for:', animal);

    return this.emojiService.getAnimalEmoji(animal);
  }

  public get value(): number {
    return Math.random();
  }
}
