import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-view-model-example',
  template: `<app-animal-listing></app-animal-listing>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewModelExampleComponent {}
