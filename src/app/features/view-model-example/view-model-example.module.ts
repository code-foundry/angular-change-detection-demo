import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';

import { AnimalListingComponent } from './animal-listing.component';
import { EmojiService } from './emoji.service';
import { ViewModelExampleComponent } from './view-model-example.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: ViewModelExampleComponent }]), CommonModule, MatButtonModule],
  declarations: [AnimalListingComponent, ViewModelExampleComponent],
  providers: [EmojiService],
})
export class ViewModelExampleModule {}
